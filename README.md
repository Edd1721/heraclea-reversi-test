# *Reversi* **GAME**

Juego de tablero Reversi.

## Configuración

1. Descarga el proyecto o clona el repositorio.
2. Completado el paso anterior instala las dependencias con **npm install**
3. Inicia el servidor con **npm start**
4. Ingresa a la URL **http://localhost:4000/** para ingresar al juego.
5. Empieza a jugar. Enjoy it!
