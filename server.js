const express = require('express')

const app = express()
const port = process.env.PORT || 4000

app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html')
app.set('views', `${__dirname}/public`)
app.use(express.static(`${__dirname}/public`))

app.get('*', (req, res) => {
  res.render('index')
})

app.listen (port, () => {
  console.log(`Aplicación corriendo en el puerto ${port}. Navega a http://localhost:4000`)
})