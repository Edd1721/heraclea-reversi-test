const gulp = require('gulp')
const webpack = require('webpack')
const pump = require('pump')
const sass = require('gulp-sass')
const csso = require('gulp-csso')
const rename = require('gulp-rename')
const concat = require('gulp-concat')
const babel = require('gulp-babel')

gulp.task('css', (callback) => {
  pump([
    gulp.src(['./assets/css/**/*.css', './assets/css/**/*.scss'])
    ,sass()
    ,concat('styles.css')
    ,csso()
    ,gulp.dest('./public')
  ], callback)
})

gulp.task('libs', (callback) => {
  pump([
    gulp.src([
      './assets/js/jquery-3.2.0.min.js'
      ,'./assets/js/materialize.js'
      ,'./assets/js/angular.min.js'
      ,'./assets/js/angular-animate.min.js'
      ,'./assets/js/angular-aria.min.js'
      ,'./assets/js/angular-route.min.js'
      ,'./assets/js/angular-resource.min.js'
    ])
    ,concat('libs.js')
    ,gulp.dest('./public')
  ], callback)
})

gulp.task('componentes', (callback) => {
  pump([
    gulp.src([
      './Angular/componentes/reversi/reversi.modulo.js'
      ,'./Angular/componentes/reversi/reversi.component.js'
      ,'./Angular/factories/reversi/reversi.factory.module.js'
      ,'./Angular/factories/reversi/reversi.factory.js'
      ,'./Angular/reversiApp.js'
      ])
      , babel({presets:['es2015']})
      ,concat('components.js')
      ,gulp.dest('./public')
  ], callback)
})

gulp.task('fonts', (callback) => {
  pump([
    gulp.src('./assets/fonts/**/*.*')
    ,gulp.dest('./public/fonts')
  ], callback)
})

gulp.task('html', (callback) => {
  pump([
    gulp.src([
       './views/*.html'
      ,'./Angular/componentes/**/*.html'
      ])
    ,gulp.dest('./public')
  ], callback)
})

gulp.task('default', ['css', 'html', 'componentes', 'libs', 'fonts'])

gulp.task('watch', () => {
  gulp.watch(['./assets/css/**/*.css', './assets/css/**/*.scss'], ['css'])
  gulp.watch(['./views/*.html','./Angular/componentes/**/*.html'], ['html'])
  gulp.watch('./Angular/reversiApp.js', ['reversiApp'])
  gulp.watch('./Angular/componentes/**/*.js', ['componentes'])
})