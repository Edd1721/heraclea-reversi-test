'use strict'

angular.module('reversi.factory')
.factory('Game', ['$resource', ($resource) => {
  return $resource('http://34.213.192.159:9000/reversi/game/:movements/', {}, {
    movements: { method: 'POST', params: {movements: 'movements', token:'9b4702ae-0861-496b-8c2e-95a9744cf583'} },
    getGame: { method: 'GET', params: {token:'9b4702ae-0861-496b-8c2e-95a9744cf583'} },
    delete: { method: 'DELETE', params: {token:'9b4702ae-0861-496b-8c2e-95a9744cf583'} }
  })
}])