angular.module('reversiApp',
 [
    'ngRoute'
   ,'reversi'
   ,'reversi.factory'
 ])
.config(['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) => {
  $routeProvider.
    when('/', {
      template: '<reversi></reversi>'
    })
    .otherwise({
      redirectTo: '/'
    })

    $locationProvider.html5Mode(true);
}])
.factory('ServiceFactory', ['$http', ($http) => {
  return function (entity, serviceUrl) {

        return new Promise(function (resolve, reject) {
            $http.post(serviceUrl, entity).then(resolve, reject);
        });
    }
}])